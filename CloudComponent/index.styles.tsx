import styled from 'styled-components';

export const CloudComponentStyled = styled.div`
  .scene {
    margin: 0 auto;
    position: relative;
    overflow: hidden;
  }
  .scene .sphere {
    position: absolute;
    top: 50%;
    left: 50%;
    width: 100px;
    height: 100px;
    margin-top: -50px;
    margin-left: -50px;
    transform-style: preserve-3d;
  }
  .scene .container {
    transform-style: preserve-3d;
    width: 100%;
    height: 100%;
    transition: transform 1s ease-in-out 0s;
  }
  .scene .container:hover {
    animation-play-state: paused;
  }
  .rotateY {
    animation: rotateY 25s infinite linear;
  }
  .rotateX {
    animation: rotateX 25s infinite linear;
  }
  .rotateZ {
    animation: rotateZ 25s infinite linear;
  }
  .translateZ.rotateY {
    animation: translateZ 0.5s ease-out, rotateY 25s 0.5s infinite linear;
  }
  .rotateY-item {
    transform-style: preserve-3d;
    transform: rotateY(85deg);
  }

  .rotateY-item:hover {
    transform-style: preserve-3d;
    transform: rotateY(0deg);
  }

  @keyframes rotateY {
    from {
      transform: rotateY(0deg);
    }
    to {
      transform: rotateY(360deg);
    }
  }

  @keyframes rotateX {
    from {
      transform: rotateX(0deg);
    }
    to {
      transform: rotateX(360deg);
    }
  }
  @keyframes rotateZ {
    from {
      transform: rotateZ(0deg);
    }
    to {
      transform: rotateZ(360deg);
    }
  }
  @keyframes translateZ {
    0% {
      transform: translateZ(-1000px);
    }
    70% {
      transform: translateZ(20px);
    }
    100% {
      transform: translateZ(0);
    }
  }
  .paused {
    animation-play-state: paused;
  }

  .sphere-column {
    position: absolute;
    display: table;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    background-color: transparent;
    overflow: hidden;
    transition: transform 1s ease-in-out 0s;
  }
  .sphere-column .column-content {
    display: table-cell;
    vertical-align: middle;
    text-align: center;
    height: 100%;
    width: 100%;
    overflow: hidden;
    background-color: rgba(255, 255, 255, 0.5);
    color: #fff;
    border-radius: 50%;
  }
  .sphere-centre {
    display: table;
    position: absolute;
    height: 100%;
    width: 100%;
    color: #ffffff;
  }
  .sphere-centre .centre-content {
    display: table-cell;
    vertical-align: middle;
    text-align: center;
    height: 100%;
    width: 100%;
    overflow: hidden;
  }
  .transparent .column-content {
    background-color: transparent;
    color: rgba(255, 255, 255, 0.5);
  }
  .circles .column-content {
    border-radius: 100px;
  }
`;
