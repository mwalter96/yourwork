export type SkillType = {
  position: {
    x: number;
    y: number;
    z: number;
  };
  title: string;
  url: string;
};

export const skills1: SkillType[] = [
  {
    position: {
      x: 0,
      y: 0,
      z: 260,
    },
    title: 'Typescript',
    url: '/',
  },
  {
    position: {
      x: 0,
      y: 30,
      z: 260,
    },
    title: 'Typescript',
    url: '/',
  },
  {
    position: {
      x: 0,
      y: 60,
      z: 260,
    },
    title: 'Typescript',
    url: '/',
  },
  {
    position: {
      x: 0,
      y: 90,
      z: 260,
    },
    title: 'Typescript',
    url: '/',
  },
  {
    position: {
      x: 0,
      y: 120,
      z: 260,
    },
    title: 'Typescript',
    url: '/',
  },
  {
    position: {
      x: 0,
      y: 150,
      z: 260,
    },
    title: 'Typescript',
    url: '/',
  },
  {
    position: {
      x: 0,
      y: 180,
      z: 260,
    },
    title: 'Typescript',
    url: '/',
  },
  {
    position: {
      x: 0,
      y: 210,
      z: 260,
    },
    title: 'Typescript',
    url: '/',
  },
  {
    position: {
      x: 0,
      y: 240,
      z: 260,
    },
    title: 'Typescript',
    url: '/',
  },
  {
    position: {
      x: 0,
      y: 270,
      z: 260,
    },
    title: 'Typescript',
    url: '/',
  },
  {
    position: {
      x: 0,
      y: 300,
      z: 260,
    },
    title: 'Typescript',
    url: '/',
  },
  {
    position: {
      x: 0,
      y: 330,
      z: 260,
    },
    title: 'Typescript',
    url: '/',
  },
];
