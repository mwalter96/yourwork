import { type } from 'os';
import React, { useRef, useState } from 'react';
import { CloudComponentStyled } from './index.styles';
import { skills1, SkillType } from './skillsList';

type Props = {
  texts: string[];
};

const getColumns = (): SkillType[][] => {
  const secondRow = [0, 1, 5, 6, 7, 11];
  const thirdRow = [1, 2, 4, 5, 6, 7, 8, 10, 11];
  const fourdRow = [0, 1, 5, 6, 7, 11];
  const fiveRow = [1, 2, 4, 5, 6, 7, 8, 10, 11];
  const sixRow = [0, 1, 5, 6, 7, 11];
  return [[...skills1]];
};

const CloudComponent = ({ texts }: Props) => {
  const mySphere = useRef<HTMLDivElement | null>(null);
  const [weights] = useState(() => getColumns());

  console.log(weights);

  return (
    <CloudComponentStyled>
      <div>
        <div
          className='scene'
          style={{ minWidth: '600px', minHeight: '600px', perspective: '800px' }}
        >
          <div className='sphere translateZ rotateY'>
            <div
              className='container'
              style={{
                transform:
                  'translateX(0px) translateY(0px) translateZ(0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg)',
              }}
            >
              <div className='sphere-centre'>
                <div className='centre-content'>
                  <h1>My Content</h1>
                </div>
              </div>
              {/** spheres */}
              {weights.map((columns, rowIndex) =>
                columns.map((element, columnIndex) => (
                  <div
                    className={`sphere-column row-${rowIndex + 1} col-${columnIndex}`}
                    style={{
                      transform: `rotateX(${element.position.x}deg) rotateY(${element.position.y}deg) translateZ(${element.position.z}px)`,
                    }}
                    key={`${rowIndex}${columnIndex}`}
                  >
                    <div
                      className='column-content rotateY-item'
                      data-cell-index={`${rowIndex * 10 + columnIndex}`}
                    >
                      {element.title}
                    </div>
                  </div>
                ))
              )}
            </div>
          </div>
        </div>
      </div>
    </CloudComponentStyled>
  );
};

export default CloudComponent;
