import { StateProps, StyleProps } from '.';

let nodes: any;
let state: any;

const defaults = () => {
  return {
    columns: 12,
    rows: 6,
    radius: 300,
    perspective: 800,
    translateX: 0,
    translateY: 0,
    translateZ: 0,
    rotateX: 0,
    rotateY: 0,
    rotateZ: 0,
    animation: 'rotateY',
    sphereClass: null,
    containerClass: null,
    poleCaps: [90, 270], //pole caps (degree) .5* PI, 1.5 * PI;
  };
};

const Style: StyleProps = {
  el: null,
  props: {},
  prefixes: ['ms', 'Moz', 'Webkit'], //opera is now Webkit

  test: function (prop) {
    if (!this.el) {
      this.el = document.createElement('div');
    }
    if (typeof this.el.style[prop] !== 'undefined') {
      this.props[prop] = prop;
      return prop;
    }
    const slug = prop[0].toUpperCase() + prop.slice(1);
    let _prop: number;
    for (var i = 0; i < this.prefixes.length; i++) {
      _prop = ((this.prefixes[i] as unknown) + slug) as number;
      if (typeof this.el.style[_prop] !== 'undefined') {
        this.props[prop] = _prop;
        return _prop;
      }
    }
    this.props[prop] = false;

    return false;
  },

  // ie < edge test
  supportsPreserve3d: function () {
    if (!this.test('transformStyle')) {
      return false;
    }
    var prop = this.prefix('transformStyle');
    var el = document.createElement('div');
    el.style[prop] = 'preserve-3d';
    // browsers set only EUNUM (valid) values
    return typeof el.style[prop] !== 'undefined' && el.style[prop] === 'preserve-3d';
  },

  // get prefix if if exist, test fails the original prop will be returned
  prefix: function (prop) {
    var test;
    if (typeof Style.props[prop] === 'undefined') {
      test = Style.test(prop);
    }
    test = Style.props[prop];
    return test ? test : prop;
  },
};

//shorthand
const prefix = Style.prefix;

const merge = (options: any) => {
  Object.keys(options).forEach((key) => {
    if (options.hasOwnProperty(key)) {
      state[key] = options[key];
    }
  });
  return state;
};

const ok = (warn: any, errors: string[]): boolean => {
  warn = warn || false;
  if (errors.length) {
    if (warn) {
      console.warn('css3sphere: not supported by this browser: ' + errors.join(', '));
    }
    return false;
  }
  return true;
};

const nodeClasses = (node: any, action: any, classes: any) => {
  if (!classes) {
    return false;
  }

  if (typeof node === 'string' && nodes) {
    node = typeof nodes[node] !== 'undefined' ? nodes[node] : null;
  }

  if (typeof node.classList === 'undefined') {
    return false;
  }

  classes = classes.split(' ');
  for (var i = 0; i < classes.length; i++) {
    if (!classes[i]) {
      continue;
    }
    switch (action) {
      case 'add':
        node.classList.add(classes[i]);
        break;
      case 'remove':
        node.classList.remove(classes[i]);
        break;
      case 'toggle':
        node.classList.toggle(classes[i]);
        break;
      default:
        return false;
    }
  }
  return node.className;
};

/* const reset = () => {
  state = defaults();
  draw();
};

const updateState = (values: any) => {
  if (Object.prototype.toString.call(values) !== '[object Object]') {
    return false;
  }

  var reference = this.defaults();
  var refType;
  var valType;

  // type casting to defaults
  for (var key in values) {
    if (!values.hasOwnProperty(key)) {
      continue;
    }
    if (!reference.hasOwnProperty(key)) {
      continue;
    }

    refType = Object.prototype.toString.call(reference[key]);
    valType = Object.prototype.toString.call(values[key]);

    if (valType === refType && refType !== '[object Number]') {
      continue;
    }

    if (refType === '[object Number]') {
      values[key] = parseInt(values[key], 10);
      if (isNaN(values[key])) {
        delete values[key];
      }
    }
  }

  merge(values);
  draw();
}; */

const isDisplayableColumn = (rowIndex: number, columnYangle: number) => {
  var isPoleCap = state.poleCaps.indexOf(columnYangle) > -1;

  if (isPoleCap && rowIndex !== 0) {
    return false;
  }

  var isPolar = false;
  for (var i = 0; i < state.poleCaps.length; i++) {
    isPolar = Math.abs(state.poleCaps[i] - columnYangle) <= 30;
    if (isPolar) {
      break;
    }
  }

  if (isPolar && rowIndex % 2) {
    return false;
  }

  return true;
};

const renderColumnCell = (rowIndex: number) => {
  var cell = document.createElement('div');
  var index = nodes.columns.push(cell);
  cell.className = 'column-content';
  cell.dataset.cellIndex = index;
  return cell;
};

const renderColumn = (
  rowIndex: number,
  columnIndex: number,
  _rotate: any,
  translate: any
): false | HTMLDivElement => {
  var rotate = {
    x: rowIndex * _rotate.row,
    y: columnIndex * _rotate.column,
  };

  if (!isDisplayableColumn(rowIndex, rotate.y)) {
    return false;
  }

  var column = document.createElement('div');
  column.className = 'sphere-column row-' + rowIndex + ' col-' + columnIndex;
  column.style[prefix('transform')] =
    'rotateX(' +
    rotate.x +
    'deg) rotateY(' +
    rotate.y +
    'deg) translateZ(' +
    Math.round(translate.z) +
    'px)';

  var cell = renderColumnCell(rowIndex);
  column.appendChild(cell);

  return column;
};

const sphereTransforms = (property?: any, value?: any) => {
  if (typeof property !== 'undefined' && state.hasOwnProperty(property)) {
    state[property] = value;
  }

  let styles: any[] = [];
  const transform = (values: string[]) => {
    const unit = values[0];
    const r = [];
    for (var i = 1; i < values.length; i++) {
      r.push(values[i] + '(' + state[values[i]] + unit + ')');
    }
    return r;
  };

  styles = styles.concat(transform(['px', 'translateX', 'translateY', 'translateZ']));
  styles = styles.concat(transform(['deg', 'rotateX', 'rotateY', 'rotateZ']));

  if (styles.length) {
    nodes.container.style[prefix('transform')] = styles.join(' ');
  }

  return true;
};

const sphereAnimation = (animation?: any) => {
  if (typeof animation === 'undefined') {
    animation = state.animation;
  }

  if (state.animation && animation === 'stop') {
    nodeClasses('sphere', 'add', 'paused');
    return;
  }

  if (state.animation && animation === 'play') {
    nodeClasses('sphere', 'remove', 'paused');
    return;
  }

  if (animation !== state.animation) {
    nodeClasses('sphere', 'remove', state.animation);
  }

  state.animation = animation;

  if (state.animation) {
    nodeClasses('sphere', 'add', state.animation);
  }
};

const sphereClass = (sphereClass?: any) => {
  if (state.sphereClass) {
    nodeClasses('sphere', 'remove', state.sphereClass);
  }

  if (typeof sphereClass !== 'undefined') {
    state.sphereClass = sphereClass;
  }

  if (state.sphereClass) {
    nodeClasses('sphere', 'add', state.sphereClass);
  }
};

const containerClass = (containerClass?: any) => {
  if (state.containerClass) {
    nodeClasses('container', 'remove', state.containerClass);
  }

  if (typeof containerClass !== 'undefined') {
    state.containerClass = containerClass;
  }

  if (state.containerClass) {
    nodeClasses('container', 'add', state.containerClass);
  }
};

const columnContent = (node: any, index: number) => {
  index = typeof index === 'number' ? index : -1;
  const columns = nodes.columns;

  const addNode = (index: number, node: any) => {
    if (typeof columns[index] === 'undefined') {
      return -1;
    }

    while (columns[index].firstChild) {
      columns[index].removeChild(columns[index].firstChild);
    }

    if (typeof node === 'string') {
      columns[index].innerHTML = node;
    } else {
      columns[index].appendChild(node);
    }

    return index;
  };

  // single
  if (index > -1) {
    return addNode(index, node);
  }

  // all
  for (var i = 0; i < columns.length; i++) {
    addNode(i, node);
  }
  return columns.length - 1;
};

export const centreContent = (node: any) => {
  if (nodes.centre.firstChild) {
    while (nodes.centre.firstChild) {
      nodes.centre.removeChild(nodes.centre.firstChild);
    }
  }

  if (!node) {
    return nodes.centre; //cleared
  }

  var content = document.createElement('div');
  content.className = 'centre-content';
  nodes.centre.appendChild(content);

  if (typeof node === 'string') {
    content.innerHTML += node;
  } else {
    content.appendChild(node);
  }
};

const line = (width: string, color: string) => {
  color = color || 'rgb(255, 255, 255)';

  var node = document.createElement('div');
  node.className = 'sphere-line';
  node.style.position = 'absolute';
  node.style.width = width;
  node.style.left = '0';
  node.style.top = '50%';
  node.style.height = '1px';
  node.style.borderBottomWidth = '1px';
  node.style.borderBottomStyle = 'solid';
  node.style.borderBottomColor = color;
  node.style[prefix('backfaceVisibility')] = 'visible';

  return node;
};

const coordinates = () => {
  var lineWidth = '100%';
  var x = line(lineWidth, 'red');
  centreContent(x);

  var y = line(lineWidth, 'blue');
  y.style[prefix('transform')] = 'rotateY(90deg)';
  centreContent(y);

  var z = line(lineWidth, 'green');
  z.style[prefix('transform')] = 'rotateZ(90deg)';
  centreContent(z);

  return nodes.centre;
};

const draw = (contents: any, errors: string[]) => {
  if (!ok(undefined, errors)) {
    return;
  }

  contents = contents || [];
  //flush
  nodes.columns = [];
  nodes.container.innerHTML = '';

  nodes.scene.style.minWidth = 2 * state.radius + 'px';
  nodes.scene.style.minHeight = 2 * state.radius + 'px';
  nodes.container.appendChild(nodes.centre);
  const rotate = {
    row: 360 / state.columns,
    column: 360 / 2 / state.rows,
  };

  const translate = {
    z: state.radius / 2 / Math.tan((rotate.column * Math.PI) / 180),
  };

  var column;
  var content;

  for (var r = 0; r < state.rows; r++) {
    for (var c = 0; c < state.columns; c++) {
      column = renderColumn(r, c, rotate, translate);
      if (column) {
        nodes.container.appendChild(column);
        if (contents.length) {
          content = contents.shift();
          columnContent(content, nodes.columns.length - 1);
        }
      }
    }
  }

  nodes.scene.style[prefix('perspective')] = state.perspective + 'px';

  containerClass();
  sphereClass();
  sphereTransforms();
  sphereAnimation();
};

const Sphere = (wrapper: HTMLDivElement, options: any) => {
  state = defaults() as StateProps;

  const css3 = ['transform', 'transformStyle', 'transition', 'backfaceVisibility', 'perspective'];
  const errorCss3 = css3.reduce((cssError, cssElement) => {
    return Style.test(cssElement) ? [...cssError] : [...cssError, cssElement];
  }, [] as string[]);

  const errors = !Style.supportsPreserve3d()
    ? [...errorCss3, 'transformStyle: preserve-3d']
    : errorCss3;

  merge(options || {});

  nodes = {
    wrapper: wrapper,
    scene: document.createElement('div'),
    sphere: document.createElement('div'),
    container: document.createElement('div'),
    centre: document.createElement('div'),
    columns: [],
  };

  nodes.scene.className = 'scene';
  nodes.sphere.className = 'sphere';
  nodes.container.className = 'container';
  nodes.centre.className = 'sphere-centre';

  nodes.wrapper.appendChild(nodes.scene);
  nodes.scene.appendChild(nodes.sphere);
  nodes.sphere.appendChild(nodes.container);
  nodes.container.appendChild(nodes.centre);

  if (ok(true, errors)) {
    draw(undefined, errors);
  }
};

export default Sphere;
