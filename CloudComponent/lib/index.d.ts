export type StyleProps = {
  el: HTMLDivElement | null;
  props: any;
  prefixes: string[];
  test: (prop: any) => boolean;
  supportsPreserve3d: () => boolean;
  prefix: (prop: any) => any;
};

export type NodeProps = {
  wrapper: HTMLDivElement;
  scene: HTMLDivElement;
  sphere: HTMLDivElement;
  container: HTMLDivElement;
  centre: HTMLDivElement;
  columns: [];
};

export type StateProps = {
  columns: number;
  rows: number;
  radius: number;
  perspective: number;
  translateX: number;
  translateY: number;
  translateZ: number;
  rotateX: number;
  rotateY: number;
  rotateZ: number;
  animation: string;
  sphereClass: null;
  containerClass: null;
  poleCaps: number[];
};
