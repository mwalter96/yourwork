import React from 'react';
import CloudComponent from '../../CloudComponent';
import { SkillsStyled } from './index.styles';

const texts: string[] = [
  '3D',
  'TagCloud',
  'JavaScript',
  'CSS3',
  'Animation',
  'Interactive',
  'Mouse',
  'Rolling',
  'Sphere',
  '6KB',
  'v2.x',
];

const Skills = () => {
  return (
    <SkillsStyled>
      <div className='w-6/12'>skills</div>
      <div className='w-6/12'>
        <CloudComponent texts={texts} />
      </div>
    </SkillsStyled>
  );
};

export default Skills;
