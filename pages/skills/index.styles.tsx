import styled from 'styled-components';

export const SkillsStyled = styled.div`
  min-height: 100vh;
  min-width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #111;
  div {
    display: flex;
    justify-content: center;
  }
`;

export default SkillsStyled;
