import type { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useContext, useEffect } from 'react';
import Interview from '../../components/Interview';
import { SocketsContext } from '../../components/sockets/SocketsContext';

const Conferences: NextPage = () => {
  const router = useRouter();
  const { id } = router.query;
  const {
    call,
    callAccepted,
    callEnded,
    userVideo,
    myVideo,
    stream,
    name,
    setName,
    me,
    callUser,
    leaveCall,
    answerCall,
  } = useContext(SocketsContext);

  useEffect(() => {
    callUser && id && me && callUser(id);
  }, [id, me]);

  return (
    <div className='bg-blue-500'>
      {id}
      <div className='flex flex-col justify-center items-center min-h-screen min-w-full'>
        <div>
          <Interview />
        </div>
      </div>
    </div>
  );
};

export default Conferences;
