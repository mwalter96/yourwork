import type { NextPage } from 'next';
import Link from 'next/link';
import Record from '../components/Record';

const Home: NextPage = () => {
  return (
    <div className='bg-blue-500'>
      <div className='flex flex-col justify-center items-center min-h-screen min-w-full'>
        <Link href='/about'>
          <a>About me</a>
        </Link>
        <Link href='/skills'>
          <a>Skills and experiencie</a>
        </Link>
        <div>
          <Record />
        </div>
      </div>
    </div>
  );
};

export default Home;
