import React, { useContext, useEffect, useRef, useState } from 'react';
import { SocketsContext } from './sockets/SocketsContext';

const Record = () => {
  const [isRecording, setIsRecording] = useState(false);
  const mediaRecorder = useRef<MediaRecorder | null>(null);
  const intervalRef = useRef<any>();
  const {
    sendChuncks,
    answerCall,
    call,
    callAccepted,
    callEnded,
    userVideo,
    myVideo,
    stream,
    name,
    me,
  } = useContext(SocketsContext);
  const chucksRef = useRef<any[]>([]);

  const toggleRecording = () => {
    setIsRecording(!isRecording);
  };

  useEffect(() => {
    if (isRecording) {
      if (myVideo?.current && stream) {
        mediaRecorder.current = new MediaRecorder(stream);
        mediaRecorder.current.start();
        mediaRecorder.current.ondataavailable = (event: BlobEvent) => {
          chucksRef.current.push(event.data);
          console.log('send = ', chucksRef.current);
          sendChuncks && sendChuncks(event.data);
        };
      }
      intervalRef.current = setInterval(() => {
        mediaRecorder.current?.requestData();
      }, 1000);
    } else {
      if (myVideo?.current && mediaRecorder.current) {
        mediaRecorder.current.stop();
        intervalRef.current && clearInterval(intervalRef.current);
      }
    }
  }, [isRecording]);

  useEffect(() => {
    console.log({ call });
    if (call?.isRecivinCall) {
      answerCall && answerCall();
    }
  }, [call]);

  return (
    <>
      <div className='w-100 flex justify-center bg-orange-500'>
        <div className='w-3/6'>
          {me}
          {stream && (
            <div>
              <p>{name}</p>
              <video playsInline muted ref={myVideo} autoPlay />
            </div>
          )}
        </div>
        <div className='w-3/6'>
          {
            <div>
              <p>{call?.name} other people</p>
              <video playsInline muted ref={userVideo} autoPlay />
            </div>
          }
        </div>
      </div>
      <button onClick={toggleRecording}>
        {isRecording ? 'Stop recording' : 'Start Recording'}
      </button>
    </>
  );
};

export default Record;
