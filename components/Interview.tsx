import React, { useContext } from 'react';
import { SocketsContext } from './sockets/SocketsContext';

const Interview = () => {
  const {
    call,
    callAccepted,
    callEnded,
    userVideo,
    myVideo,
    stream,
    name,
    setName,
    me,
    callUser,
    leaveCall,
    answerCall,
  } = useContext(SocketsContext);

  return (
    <div className='w-100 flex justify-center bg-orange-500'>
      <div className='w-3/6'>
        {me}
        {stream && (
          <div>
            <p>{name}</p>
            <video playsInline muted ref={myVideo} autoPlay />
          </div>
        )}
      </div>
      <div className='w-3/6'>
        {callAccepted && !callEnded && (
          <div>
            <p>{call?.name} other people</p>
            <video playsInline muted ref={userVideo} autoPlay />
          </div>
        )}
      </div>
    </div>
  );
};

export default Interview;
