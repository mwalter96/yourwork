import React, { createContext, useEffect, useRef, useState } from 'react';
import { io } from 'socket.io-client';
import Peer from 'simple-peer';

const socket = io('https://video-test-strainer.herokuapp.com');

type CallType = { isRecivinCall: boolean; from: any; name: any; signal: any };
const initialCall: CallType = {
  isRecivinCall: false,
  from: '',
  name: '',
  signal: '',
};
type ContextType = {
  call: CallType;
  callAccepted: boolean;
  callEnded: boolean;
  userVideo: React.RefObject<HTMLVideoElement>;
  myVideo: React.RefObject<HTMLVideoElement>;
  stream: MediaStream | undefined;
  name: string;
  setName: React.Dispatch<React.SetStateAction<string>>;
  me: any;
  callUser: (id: any) => void;
  leaveCall: () => void;
  answerCall: () => void;
  sendChuncks: (data: Blob) => void;
};
export const SocketsContext = createContext<Partial<ContextType>>({});
const ContextProvider = ({ children }: any) => {
  const [stream, setStream] = useState<MediaStream>();
  const [me, setMe] = useState<any>();
  const [name, setName] = useState('');
  const [call, setCall] = useState<CallType>(initialCall);
  const [callAccepted, setCallAccepted] = useState(false);
  const [callEnded, setCallEnded] = useState(false);
  const myVideo = useRef<HTMLVideoElement>(null);
  const userVideo = useRef<HTMLVideoElement>(null);
  const connectionRef = useRef<Peer.Instance>();

  const getVideo = () => {
    try {
      navigator.mediaDevices
        .getUserMedia({
          video: { width: 480, height: 480, facingMode: 'user' },
          audio: true,
        })
        .then((currentStream) => {
          setStream(currentStream);
          if (myVideo.current) myVideo.current.srcObject = currentStream;
        });
      socket.on('me', (socketId) => {
        setMe(socketId);
      });
      socket.on('calluser', ({ from, name: callerName, signal }: any) => {
        console.log({ calluser: 'calluser', isRecivinCall: true, from, name: callerName, signal });
        setCall({ isRecivinCall: true, from, name: callerName, signal });
      });
    } catch (error) {
      console.error({ error });
    }
  };
  useEffect(() => {
    getVideo();
  }, []);

  const answerCall = () => {
    setCallAccepted(true);
    const peer = new Peer({ initiator: false, trickle: false, stream });
    peer.on('signal', (data) => {
      console.log('answerCall', { signal: data, to: call.from });
      socket.emit('answerCall', { signal: data, to: call.from });
    });
    peer.on('stream', (currentStream) => {
      console.log({ currentStream, currentVideo: userVideo.current });
      if (userVideo.current) userVideo.current.srcObject = currentStream;
    });
    peer.signal(call.signal);
    console.log({ reply: call });
    connectionRef.current = peer;
  };
  const callUser = (id: any) => {
    console.log(id);
    const peer = new Peer({ initiator: true, trickle: false, stream });
    peer.on('signal', (data) => {
      console.log('calluser', { userToCall: id, signalData: data, from: me, name });
      socket.emit('calluser', { userToCall: id, signalData: data, from: me, name });
    });
    peer.on('stream', (currentStream) => {
      if (userVideo.current) userVideo.current.srcObject = currentStream;
    });
    socket.on('callAccepted', (signal) => {
      setCallAccepted(true);
      peer.signal(signal);
      console.log({ signal });
    });
    connectionRef.current = peer;
  };
  const leaveCall = () => {
    setCallEnded(true);
    connectionRef.current?.destroy();
    window.location.reload();
  };

  const sendChuncks = (data: Blob) => {
    socket.emit('saveVideo', data);
  };

  return (
    <SocketsContext.Provider
      value={{
        call,
        callAccepted,
        callEnded,
        userVideo,
        myVideo,
        stream,
        name,
        setName,
        me,
        callUser,
        leaveCall,
        answerCall,
        sendChuncks,
      }}
    >
      {children}
    </SocketsContext.Provider>
  );
};

export default ContextProvider;
